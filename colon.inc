%ifnmacro colon 2
    %define dict_head 0

    %macro colon 2
    __dict_%2:     ; "инкапсулирую" лейбл, чтобы не было коллизий лейблов
        dq dict_head
        db %1, 0
    %define dict_head __dict_%2
    %endmacro

%endif
