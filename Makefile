ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.PHONY: build
build: main

main.o: main.asm colon.inc words.inc lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $?

.PHONY: clean
clean:
	rm main *.o
