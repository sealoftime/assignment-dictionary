%include "lib.inc"
extern find_word
global _start

section .data
%include "colon.inc"
%include "words.inc"

MSG_PROMPT: db "Key: ", 0
MSG_FOUND:  db "Value: ", 0
MSG_ERR_1:  db "Value with key '", 0
MSG_ERR_2:  db "' not found", 0
MSG_READ_ERR: db "Error while reading key.", 0

%define BUF_SIZE 256

section .text
_start:
    mov rdi, MSG_PROMPT
    call print_string
 
    sub rsp, BUF_SIZE
    mov rdi, rsp
    mov rsi, BUF_SIZE
    call read_line
    test rax, rax
    jz .read_err

    push rsp
    mov rdi, [rsp]
    mov rsi, dict_head
    call find_word
    test rax, rax
    jz .not_found
.found:
    pop rdi
    add rax, 8         ; Пропустим ссылку на след. элемент списка
    push rax
    call string_length
    inc rax            ; +1 за нулль-терминатор
    add [rsp], rax     ; Теперь вершина стека указывает на Value записи
    mov rdi, MSG_FOUND
    call print_string
    pop rdi
    call print_string
    call print_newline
    call exit
.not_found:
    mov rdi, MSG_ERR_1
    call print_string
    pop rdi
    call print_string
    mov rdi, MSG_ERR_2
    call print_string
    call print_newline
    mov rdi, -1
    call exit
.read_err:
    mov rdi, MSG_READ_ERR
    call print_string
    call print_newline
    mov rdi, -1
    call exit
