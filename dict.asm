
section .text
%include "lib.inc"

global find_word

; find_word принимает указатель на начало строки-ключа и указатель на начало
; словаря.
; Возвращает: указатель на начало вхождения в словарь или 0, если не нашлось
; соответствие ключу.
find_word:
    push rdi           ; Спасаем rdi от пагубного влияния string_equals
    push rsi
    add rsi, 8         ; Пропускаем указатель на следующий элемент списка
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .ret           ; Нашли
    mov rsi, [rsi]
    test rsi, rsi
    jz .ret            ; Достигли конца
    jmp find_word
.ret:
    mov rax, rsi
    ret
